import {
  DeesElement,
  customElement,
  TemplateResult,
  html,
  property,
  css,
  cssManager,
} from '@designestate/dees-element';

import * as domtools from '@designestate/dees-domtools';

enum ETestEnum {
  'first' = 'first',
  'second' = 'second',
  'awesome' = 'awesome',
}

@customElement('test-demoelement')
export class TestDemoelement extends DeesElement {
  public static demo = () => html`<test-demoelement>This is a slot text</test-demoelement>`;

  @property()
  public notTyped = 'hello';

  @property({
    type: String,
  })
  public typedAndNotInitizalized: string;

  @property()
  public notTypedAndNotInitizalized: string;

  @property({
    type: Boolean,
  })
  public demoBoolean = false;

  @property({
    type: String,
  })
  public demoString = 'default demo string';

  @property({
    type: Number,
  })
  public demoNumber = 2;

  @property({
    type: ETestEnum,
  })
  public demoENum: ETestEnum = ETestEnum.first;

  constructor() {
    super();
  }

  public static styles = [
    css`
      .maincontainer,
      .themeindicator {
        display: block;
        padding: 10px;
        border-radius: 10px;
        margin-bottom: 20px;
      }
      .maincontainer {
        color: #fff;
        background: #000;
      }
      .themeindicator {
        color: ${cssManager.bdTheme('#000', '#fff')};
        background: ${cssManager.bdTheme('#fff', '#000')};
      }

      @container wccToolsViewport size(min-width: 1px) {
        .maincontainer,
        .themeindicator {
          border-radius: 50px;
        }
      }

      ${domtools.breakpoints.cssForPhablet(css`
        .maincontainer,
        .themeindicator {
          border-radius: 50px;
        }
      `)}

      pre b {
        color: green;
      }
    `,
  ];

  public render() {
    return html`
      <style></style>
      <div class="maincontainer"><slot>This is a demo element</slot></div>
      <div class="themeindicator">
        You have selected the ${this.goBright ? 'bright' : 'dark'} theme.
        <pre>
          demoBoolean is <b>${this.demoBoolean}</b>
          demoString is <b>"${this.demoString}"</b>
          demoNumber is <b>${this.demoNumber}</b>
          demoEnum is <b>"${this.demoENum}"</b>
        </pre
        >
      </div>
    `;
  }
}
