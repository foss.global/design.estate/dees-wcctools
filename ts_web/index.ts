import { WccDashboard } from './elements/wcc-dashboard.js';
import { LitElement, type TemplateResult } from 'lit';

const setupWccTools = (elementsArg?: { [key: string]: LitElement }, pagesArg?: { [key: string]: () => TemplateResult }) => {
  let hasRun = false;
  const runWccToolsSetup = async () => {
    if (document.readyState === 'complete' && !hasRun) {
      hasRun = true;
      const wccTools = new WccDashboard(elementsArg as any, pagesArg);
      document.querySelector('body').append(wccTools);
    }
  };
  document.addEventListener('readystatechange', runWccToolsSetup);
  runWccToolsSetup();
};

export {
  setupWccTools
};
