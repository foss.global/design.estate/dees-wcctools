/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@design.estate/dees-wcctools',
  version: '1.0.78',
  description: 'wcc tools for creating element catalogues'
}
