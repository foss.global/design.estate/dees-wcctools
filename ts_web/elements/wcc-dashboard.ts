import { DeesElement, property, html, customElement, type TemplateResult, queryAsync, render, domtools } from '@design.estate/dees-element';

import * as plugins from '../wcctools.plugins.js';

// wcc tools
import './wcc-frame.js';
import './wcc-sidebar.js';
import './wcc-properties.js';
import { type TTheme } from './wcc-properties.js';
import { type TElementType } from './wcc-sidebar.js';
import { breakpoints } from '@design.estate/dees-domtools';
import { WccFrame } from './wcc-frame.js';

@customElement('wcc-dashboard')
export class WccDashboard extends DeesElement {

  @property()
  public selectedType: TElementType;

  @property()
  public selectedItemName: string;

  @property()
  public selectedItem: (() => TemplateResult) | DeesElement;

  @property()
  public selectedViewport: plugins.deesDomtools.breakpoints.TViewport = 'desktop';

  @property()
  public selectedTheme: TTheme = 'dark';

  @property()
  public pages: { [key: string]: () => TemplateResult } = {};

  @property()
  public elements: { [key: string]: DeesElement } = {};

  @property()
  public warning: string = null;
  
  @queryAsync('wcc-frame')
  public wccFrame: Promise<WccFrame>;

  constructor(
    elementsArg?: { [key: string]: DeesElement },
    pagesArg?: { [key: string]: () => TemplateResult }
  ) {
    super();
    if (elementsArg) {
      this.elements = elementsArg;
      console.log('got elements:');
      console.log(this.elements);
    }

    if (pagesArg) {
      this.pages = pagesArg;
    }
  }

  public render(): TemplateResult {
    return html`
      <style>
        :host {
          font-family: 'Mona Sans', 'Inter', sans-serif;
          background: #fcfcfc;
          display: block;
          box-sizing: border-box;
        }
        :host([hidden]) {
          display: none;
        }
      </style>
      <wcc-sidebar
        .dashboardRef=${this}
        .selectedItem=${this.selectedItem}
        @selectedType=${(eventArg) => {
          this.selectedType = eventArg.detail;
        }}
        @selectedItemName=${(eventArg) => {
          this.selectedItemName = eventArg.detail;
        }}
        @selectedItem=${(eventArg) => {
          this.selectedItem = eventArg.detail;
        }}
      ></wcc-sidebar>
      <wcc-properties
        .dashboardRef=${this}
        .warning="${this.warning}"
        .selectedItem=${this.selectedItem}
        .selectedViewport=${this.selectedViewport}
        .selectedTheme=${this.selectedTheme}
        @selectedViewport=${(eventArg) => {
          this.selectedViewport = eventArg.detail;
          this.scheduleUpdate();
        }}
        @selectedTheme=${(eventArg) => {
          this.selectedTheme = eventArg.detail;
        }}
      ></wcc-properties>
      <wcc-frame id="wccFrame" viewport=${this.selectedViewport}>
      </wcc-frame>
    `;
  }

  public setWarning(warningTextArg: string) {
    if (this.warning !== warningTextArg) {
      console.log(warningTextArg);
      this.warning = warningTextArg;
      setTimeout(() => {
        this.scheduleUpdate();
      }, 0);
    }
  }

  public async firstUpdated() {
    this.domtools = await plugins.deesDomtools.DomTools.setupDomTools();
    this.domtools.router.on(
      '/wcctools-route/:itemType/:itemName/:viewport/:theme',
      async (routeInfo) => {
        this.selectedType = routeInfo.params.itemType as TElementType;
        this.selectedItemName = routeInfo.params.itemName;
        this.selectedViewport = routeInfo.params.viewport as breakpoints.TViewport;
        this.selectedTheme = routeInfo.params.theme as TTheme;
        if (routeInfo.params.itemType === 'element') {
          this.selectedItem = this.elements[routeInfo.params.itemName];
        } else if (routeInfo.params.itemType === 'page') {
          this.selectedItem = this.pages[routeInfo.params.itemName];
        }
        const domtoolsInstance = await plugins.deesDomtools.elementBasic.setup();
        this.selectedTheme === 'bright'
          ? domtoolsInstance.themeManager.goBright()
          : domtoolsInstance.themeManager.goDark();
      }
    );
  }

  public async updated() {
    this.domtools = await plugins.deesDomtools.DomTools.setupDomTools();
    await this.domtools.router._handleRouteState();
    const storeElement = this.selectedItem;
    const wccFrame: WccFrame = this.shadowRoot.querySelector('wcc-frame');

    if (this.selectedType === 'page' && this.selectedItem) {
      if (typeof this.selectedItem === 'function') {
        console.log('slotting page.');
        const viewport = await wccFrame.getViewportElement();
        render(this.selectedItem(), viewport);
        console.log('rendered page.');
      } else {
        console.error('The selected item looks strange:');
        console.log(this.selectedItem);
      }
    } else if (this.selectedType === 'element' && this.selectedItem) {
      console.log('slotting element.');
      const anonItem: any = this.selectedItem;
      if (!anonItem.demo) {
        this.setWarning(`component ${anonItem.name} does not expose a demo property.`);
        return;
      }
      if (!(typeof anonItem.demo === 'function')) {
        this.setWarning(
          `component ${anonItem.name} has demo property, but it is not of type function`
        );
        return;
      }
      this.setWarning(null);
      const viewport = await wccFrame.getViewportElement();
      render(anonItem.demo(), viewport);;
    }
  }

  public buildUrl() {
    this.domtools.router.pushUrl(
      `/wcctools-route/${this.selectedType}/${this.selectedItemName}/${this.selectedViewport}/${this.selectedTheme}`
    );
  }
}
