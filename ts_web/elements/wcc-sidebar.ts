import * as plugins from '../wcctools.plugins.js';
import { DeesElement, property, html, customElement, type TemplateResult } from '@design.estate/dees-element';
import { WccDashboard } from './wcc-dashboard.js';

export type TElementType = 'element' | 'page';

@customElement('wcc-sidebar')
export class WccSidebar extends DeesElement {
  @property({type: Array})
  public websites: string[] = [];

  @property({ attribute: false })
  public selectedItem: DeesElement | (() => TemplateResult);

  @property({ attribute: false })
  public selectedType: TElementType;

  @property()
  public dashboardRef: WccDashboard;

  public render(): TemplateResult {
    return html`
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
      <style>
        :host {
          display: block;
          border-right: 1px solid #999;
          font-family: 'Mona Sans', 'Inter', sans-serif;
          box-sizing: border-box;
          position: absolute;
          left: 0px;
          width: 200px;
          top: 0px;
          bottom: 0px;
          overflow-y: scroll;
          overflow-x: hidden;
          background: #222;
          color: #fff;
          padding: 5px;
        }

        .material-symbols-outlined {
          font-family: 'Material Symbols Outlined';
          font-weight: normal;
          font-style: normal;
          font-size: 24px;  /* Preferred icon size */
          display: inline-block;
          line-height: 1;
          text-transform: none;
          letter-spacing: normal;
          word-wrap: normal;
          white-space: nowrap;
          direction: ltr;
          font-variation-settings: 'FILL' 1, 'wght' 400, 'GRAD' 0, 'opsz' 48;
        }

        .heading {
          font-size: 24px;
          text-align: center;        
          margin: 20px 5px 5px 5px;
        }

        .heading a {
          text-decoration: none;
          color: #fff;
        }

        .subheading {
          text-align: center;
        }
        .selectOption {
          position: relative;
          line-height: 24px;
          padding: 5px;
          transition: all 0.2s;
          display: grid;
          grid-template-columns: 28px auto;
        }
        .selectOption:hover {
          cursor: pointer;
          padding: 5px;
          color: #333;
          background: #fff;
        }

        .selectOption.selected {
          background: #455A64;;
        }

        .selectOption.selected:hover {
          cursor: pointer;
          color: #ffffff;
          background: #455A64;
        }

        .selectOption .material-symbols-outlined {
          color: #666;
          display: block;
          transition: all 0.2s;
        }

        .selectOption.selected .material-symbols-outlined {
          color: #000;
        }

        .selectOption .text {
          display: block;
          word-wrap: break-word;
          word-break: break-all;
          max-width: 100%;
        }

        
      </style>
      <div class="heading">
        <a href="https://gitlab.com/designestate/dees-wcctools" target="_blank">wcctools</a>
      </div>
      <div class="subheading">
        by Lossless GmbH
      </div>
      <div class="menu">
        <h3>Live Websites</h3>
        ${this.websites.map(website => {
          return html`<div class="selectOption"><i class="material-symbols-outlined">ondemand_video</i><div class="text">${website}</div></div>`;
        })}
        <h3>Pages</h3>
        ${(() => {
          const pages = Object.keys(this.dashboardRef.pages);
          return pages.map(pageName => {
            const item = this.dashboardRef.pages[pageName];
            return html`
              <div
                class="selectOption ${this.selectedItem === item ? 'selected' : null}"
                @click=${async () => {
                  const domtools = await plugins.deesDomtools.DomTools.setupDomTools();
                  this.selectItem('page', pageName, item);
                }}
              >
                <i class="material-symbols-outlined">insert_drive_file</i>
                <div class="text">${pageName}</div>
              </div>
            `;
          });
        })()}
        <h3>Elements</h3>
        ${(() => {
          const elements = Object.keys(this.dashboardRef.elements);
          return elements.map(elementName => {
            const item = this.dashboardRef.elements[elementName];
            return html`
              <div
                class="selectOption ${this.selectedItem === item ? 'selected' : null}"
                @click=${async () => {
                  const domtools = await plugins.deesDomtools.DomTools.setupDomTools();
                  this.selectItem('element', elementName, item);
                }}
              >
                <i class="material-symbols-outlined">featured_video</i>
                <div class="text">${elementName}</div>
              </div>
            `;
          });
        })()}
      </menu>
    `;
  }

  public selectItem(typeArg: TElementType, itemNameArg: string, itemArg: (() => TemplateResult) | DeesElement) {
    console.log('selected item');
    console.log(itemNameArg);
    console.log(itemArg);
    this.selectedItem = itemArg;
    this.selectedType = typeArg;
    this.dispatchEvent(
      new CustomEvent('selectedType', {
        detail: typeArg
      })
    );
    this.dispatchEvent(
      new CustomEvent('selectedItemName', {
        detail: itemNameArg
      })
    );
    this.dispatchEvent(
      new CustomEvent('selectedItem', {
        detail: itemArg
      })
    );
    
    this.dashboardRef.buildUrl();
  }
}
