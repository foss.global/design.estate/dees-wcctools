import { DeesElement, property, html, customElement, type TemplateResult, state } from '@design.estate/dees-element';
import { WccDashboard } from './wcc-dashboard.js';

export type TPropertyType = 'String' | 'Number' | 'Boolean' | 'Object' | 'Enum' | 'Array';

export type TEnvironment = 'native' | 'desktop' | 'tablet' | 'phablet' | 'phone';
export type TTheme = 'bright' | 'dark';

let environment: TEnvironment = 'native';

export const setEnvironment = (envArg) => {
  environment = envArg;
};

@customElement('wcc-properties')
export class WccProperties extends DeesElement {
  @property({
    type: WccDashboard
  })
  public dashboardRef: WccDashboard;

  @property()
  public selectedItem: (() => TemplateResult) | DeesElement;

  @property()
  public selectedViewport: TEnvironment = 'native';

  @property()
  public selectedTheme: TTheme = 'dark';

  @property()
  public warning: string = null;

  @state()
  propertyContent: TemplateResult[] = [];

  public render(): TemplateResult {
    return html`
      <style>
        :host {
          font-family: 'Roboto', sans-serif;
          box-sizing: border-box;
          position: absolute;
          left: 200px;
          height: 100px;
          bottom: 0px;
          right: 0px;
          overflow: hidden;
          background: #111;
          color: #fff;
        }
        .grid {
          display: grid;
          grid-template-columns: auto 150px 300px 70px;
        }
        .properties {
          border-right: 1px solid #999;
          height: 100px;
          overflow-y: auto;
          display: grid;
          grid-template-columns: 33% 33% 33%;
        }

        .material-symbols-outlined {
          font-family: 'Material Symbols Outlined';
          font-weight: normal;
          font-style: normal;
          font-size: 24px;  /* Preferred icon size */
          display: inline-block;
          line-height: 1;
          text-transform: none;
          letter-spacing: normal;
          word-wrap: normal;
          white-space: nowrap;
          direction: ltr;
          font-variation-settings: 'FILL' 1, 'wght' 400, 'GRAD' 0, 'opsz' 48;
        }

        .properties .property {
          padding: 5px;
          background: #444;
          border: 1px solid #000;
        }

        .properties input,
        .properties select {
          display: block;
          width: 100%;
          background: #333;
          border: none;
          color: #fff;
        }

        .viewportSelector,
        .themeSelector {
          user-select: none;
          border-right: 1px solid #999;
        }
        .selectorButtons2 {
          display: grid;
          grid-template-columns: 50% 50%;
        }
        .selectorButtons4 {
          display: grid;
          grid-template-columns: 25% 25% 25% 25%;
        }
        .button {
          padding: 10px;
          text-align: center;
          border: 1px solid #000;
          transition: all 0.2s;
        }
        .button:hover {
          cursor: pointer;
          color: #333;
          background: #fff;
        }

        .button.selected {
          background: #455a64;
        }

        .button.selected:hover {
          cursor: pointer;
          color: #ffffff;
          background: #455a64;
        }

        .panelheading {
          padding: 5px;
          font-weight: bold;
          background: #444;
          border: 1px solid #000;
        }
        .docs {
          text-align: center;
          line-height: 100px;
          text-transform: uppercase;
        }

        .docs:hover {
          cursor: pointer;
          color: #333;
          background: #fff;
        }

        .warning {
          position: absolute;
          background: #222;
          color: #CCC;
          top: 0px;
          bottom: 0px;
          left: 0px;
          right: 520px;
          text-align: center;
          padding: 35px;
          font-size: 25px;
        }
      </style>
      <div class="grid">
        <div class="properties">
          <div class="panelheading">Properties</div>
          ${this.propertyContent}
        </div>
        <div class="themeSelector">
          <div class="panelheading">Theme</div>
          <div class="selectorButtons2">
            <div
              class="button ${this.selectedTheme === 'dark' ? 'selected' : null}"
              @click=${() => {
                this.selectTheme('dark');
              }}
            >
              Dark<br /><i class="material-symbols-outlined">brightness_3</i>
            </div>
            <div
              class="button ${this.selectedTheme === 'bright' ? 'selected' : null}"
              @click=${() => {
                this.selectTheme('bright');
              }}
            >
              Bright<br /><i class="material-symbols-outlined">flare</i>
            </div>
          </div>
        </div>
        <div class="viewportSelector">
          <div class="panelheading">Viewport</div>
          <div class="selectorButtons4">
            <div
              class="button ${this.selectedViewport === 'phone' ? 'selected' : null}"
              @click=${() => {
                this.selectViewport('phone');
              }}
            >
              Phone<br /><i class="material-symbols-outlined">smartphone</i>
            </div>
            <div
              class="button ${this.selectedViewport === 'phablet' ? 'selected' : null}"
              @click=${() => {
                this.selectViewport('phablet');
              }}
            >
              Phablet<br /><i class="material-symbols-outlined">smartphone</i>
            </div>
            <div
              class="button ${this.selectedViewport === 'tablet' ? 'selected' : null}"
              @click=${() => {
                this.selectViewport('tablet');
              }}
            >
              Tablet<br /><i class="material-symbols-outlined">tablet</i>
            </div>
            <div
              class="button ${this.selectedViewport === 'desktop' ||
              this.selectedViewport === 'native'
                ? 'selected'
                : null}"
              @click=${() => {
                this.selectViewport('native');
              }}
            >
              Desktop<br /><i class="material-symbols-outlined">desktop_windows</i>
            </div>
          </div>
        </div>
        <div class="docs">Docs</div>
      </div>
      ${this.warning ? html`<div class="warning">${this.warning}</div>` : null}
    `;
  }

  public async createProperties() {
    console.log('creating properties for:');
    console.log(this.selectedItem);
    const isEnumeration = (propertyArg): boolean => {
      const keys = Object.keys(propertyArg.type);
      const values = [];
      for (const key of keys) {
        let value = propertyArg.type[key];
        if (typeof value === 'number') {
          value = value.toString();
        }
        values.push(value);
      }
      for (const key of keys) {
        if (values.indexOf(key) < 0) {
          return false;
        }
      }
      return true;
    };
    const getEnumValues = (propertyArg): any[] => {
      console.log(JSON.stringify(propertyArg));
      const enumValues: any[] = [];
      Object.keys(propertyArg.type).forEach((valueArg: string) => {
        enumValues.push(valueArg);
      });
      return enumValues;
    };
    const determinePropertyType = async (propertyArg: any): Promise<TPropertyType> => {
      const typeName: any | undefined = propertyArg.type.name;
      if (typeName) {
        return typeName;
      } else {
        return Array.isArray(propertyArg)
          ? 'Array'
          : isEnumeration(propertyArg)
          ? 'Enum'
          : 'Object';
      }
    };
    if (this.selectedItem && (this.selectedItem as any).demo) {
      console.log(`Got Dees-Element for property evaluation.`);
      const anonItem: any = this.selectedItem;
      if (!anonItem) {
        this.warning = 'no element selected';
        return;
      }
      console.log(anonItem.elementProperties);
      const wccFrame = await this.dashboardRef.wccFrame;
      let firstFoundInstantiatedElement: HTMLElement;
      for (const element of Array.from((await wccFrame.getViewportElement()).children)) {
        if (element instanceof (this.selectedItem as any)) {
          firstFoundInstantiatedElement = element as HTMLElement;
          break;
        }
      }
      if (!firstFoundInstantiatedElement) {
        this.warning = `no first instantiated element found for >>${anonItem.name}<<`;
        return;
      }
      const classProperties: Map<string, any> = anonItem.elementProperties;
      if (!classProperties) {
        this.warning = `selected element >>${anonItem.name}<< does not expose element properties`;
        return;
      }
      this.warning = null;
      const propertyArray: TemplateResult[] = [];
      for (const key of classProperties.keys()) {
        if (key === 'goBright' || key === 'domtools') {
          continue;
        }
        const property = classProperties.get(key);
        const propertyTypeString = await determinePropertyType(property);
        propertyArray.push(
          html`
            <div class="property">
              ${key} / ${propertyTypeString}<br />
              ${(() => {
                switch (propertyTypeString) {
                  case 'Boolean':
                    return html`<input
                      type="checkbox"
                      ?checked=${firstFoundInstantiatedElement[key]}
                      @input="${(eventArg: any) => {
                        firstFoundInstantiatedElement[key] = eventArg.target.checked;
                      }}"
                    />`;
                  case 'String':
                    return html`<input
                      type="text"
                      value=${firstFoundInstantiatedElement[key]}
                      @input="${(eventArg: any) => {
                        firstFoundInstantiatedElement[key] = eventArg.target.value;
                      }}"
                    />`;
                  case 'Number':
                    return html`<input
                      type="number"
                      value=${firstFoundInstantiatedElement[key]}
                      @input="${(eventArg: any) => {
                        firstFoundInstantiatedElement[key] = eventArg.target.value;
                      }}"
                    />`;
                  case 'Enum':
                    const enumValues: any[] = getEnumValues(property);
                    return html`<select
                      @change="${(eventArg: any) => {
                        firstFoundInstantiatedElement[key] = eventArg.target.value;
                      }}"
                    >
                      ${enumValues.map((valueArg) => {
                        return html`
                          <option
                            ?selected=${valueArg === firstFoundInstantiatedElement[key] ? true : false}
                            name="${valueArg}"
                          >
                            ${valueArg}
                          </option>
                        `;
                      })}
                    </select>`;
                }
              })()}
            </div>
          `
        );
      }
      this.propertyContent = propertyArray;
    } else {
      console.log(`Cannot extract properties of ${(this.selectedItem as any)?.name}`);
      this.warning = `You selected a page.`;
      return null;
    }
  }

  public selectTheme(themeArg: TTheme) {
    this.selectedTheme = themeArg;
    this.dispatchEvent(
      new CustomEvent('selectedTheme', {
        detail: themeArg,
      })
    );
    console.log(this.dashboardRef.selectedType);
    this.dashboardRef.buildUrl();
  }

  public async scheduleUpdate() {
    await this.createProperties();
    super.scheduleUpdate();
  }

  public selectViewport(viewport: TEnvironment) {
    this.selectedViewport = viewport;
    setEnvironment(viewport);
    this.dispatchEvent(
      new CustomEvent('selectedViewport', {
        detail: viewport,
      })
    );
    this.dashboardRef.buildUrl();
  }
}
