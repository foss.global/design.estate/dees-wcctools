import { DeesElement, property, html, customElement, type TemplateResult, css, cssManager } from '@design.estate/dees-element';

import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'wcc-frame': WccFrame;
  }
}

@customElement('wcc-frame')
export class WccFrame extends DeesElement {
  @property()
  public viewport: string;

  public static styles = [
    css`
      :host {
        border: 10px solid #ffaeaf;
        position: absolute;
        background: ${cssManager.bdTheme('#333', '#000')};
        left: 200px;
        right: 0px;
        top: 0px;
        bottom: 100px;
        overflow-y: auto;
        overflow-x: auto;
      }

      .viewport {
        container-type: inline-size;
        container-name: wccToolsViewport;
        position: relative;
        min-height: 100%;
      }
    `,
  ]

  public render(): TemplateResult {
    return html`
      <style>
        :host {
          ${(() => {
          switch (this.viewport) {
            case 'desktop':
              return `
                  padding: 0px 0px;
                `;
            case 'tablet':
              return `
                  padding: 0px ${
                    (document.body.clientWidth - 200 - domtools.breakpoints.tablet) / 2
                  }px;
                `;
            case 'phablet':
              return `
                  padding: 0px ${
                    (document.body.clientWidth - 200 - domtools.breakpoints.phablet) / 2
                  }px;
                `;
            case 'phone':
              return `
                  padding: 0px ${
                    (document.body.clientWidth - 200 - domtools.breakpoints.phone) / 2
                  }px;
                `;
          }
          })()}
        }

        .viewport {
          ${this.viewport !== 'desktop'
            ? html` border-right: 1px dotted #444; border-left: 1px dotted #444; `
            : html``
          }
          background:
          ${
            this.goBright ? `
              radial-gradient(#CCCCCC 3px, transparent 4px),
              radial-gradient(#CCCCCC 3px, transparent 4px),
              linear-gradient(#eeeeee 4px, transparent 0),
              linear-gradient(45deg, transparent 74px, transparent 75px, #CCCCCC 75px, #CCCCCC 76px, transparent 77px, transparent 109px),
              linear-gradient(-45deg, transparent 75px, transparent 76px, #CCCCCC 76px, #CCCCCC 77px, transparent 78px, transparent 109px),
              #eeeeee;
              background-size: 109px 109px, 109px 109px,100% 6px, 109px 109px, 109px 109px;
              background-position: 54px 55px, 0px 0px, 0px 0px, 0px 0px, 0px 0px;
            ` : `
              radial-gradient(#444444 3px, transparent 4px),
              radial-gradient(#444444 3px, transparent 4px),
              linear-gradient(#222222 4px, transparent 0),
              linear-gradient(45deg, transparent 74px, transparent 75px, #444444 75px, #444444 76px, transparent 77px, transparent 109px),
              linear-gradient(-45deg, transparent 75px, transparent 76px, #444444 76px, #444444 77px, transparent 78px, transparent 109px),
              #222222;
              background-size: 109px 109px, 109px 109px,100% 6px, 109px 109px, 109px 109px;
              background-position: 54px 55px, 0px 0px, 0px 0px, 0px 0px, 0px 0px;
            `
          }
        }
      </style>
      <div class="viewport">
        
      </div>
    `;
  }

  public async getDisplayedInstance() {
    await this.updateComplete;
    const slottedContent = this.children;
    console.log(slottedContent);
  }

  public async getViewportElement(): Promise<HTMLElement> {
    return this.shadowRoot.querySelector('.viewport') as HTMLElement;
  }
}
